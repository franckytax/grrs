# grrs : A grep clone written in Rust

The goal of this project is to write a command line app written in Rust based on the existing grep command.

## Building

Run this command in the project folder :

    cargo build

## Starting

After building the project, run this command :

    cargo run <PATTERN> <PATH>

